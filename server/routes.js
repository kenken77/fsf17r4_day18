var filesystem = require("fs"),
    path= require("path"),
    multer = require("multer"),
    AWS = require("aws-sdk"),
    multerS3 = require("multer-s3"),
    storage = multer.diskStorage({
        destination: './upload_tmp',
        filename: function(req, file, callback){
            console.log(file);
            console.log(file.originalname);
            callback(null, Date.now()+ '-' + file.originalname);
        }
    }),
    upload = multer({
        storage: storage
    });

AWS.config.region = "ap-southeast-1";
console.log("AWS init ...");
var s3bucket = new AWS.S3({

});

var uploadS3 = new multer({
    storage: multerS3({
        s3: s3bucket,
        bucket: 'kenneth-nus',
        acl: 'public-read',
        metadata: function(req, file, cb){
            console.log(file.fieldname);
            cb(null, {fieldName: file.fieldname});
        },
        key: function(req, file, cb){
            console.log(cb);
            console.log(file.originalname);
            cb(null, Date.now() + '-' + file.originalname);
        }
    })
})

module.exports = function(app){
    app.post("/upload",upload.single("img-file") ,function(req, res){
        console.log("uploading .....");
        filesystem.readFile(req.file.path, function(err, data){
            if(err){
                console.log(err);
            }
            res.status(202).json({size: req.file.size});
        });
    });

    app.post("/uploadS3",uploadS3.single("img-file") ,function(req, res){
        res.send("Successfully uploaded a file " + req.file.length)
    });
}