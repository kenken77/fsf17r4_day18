(function () {
    angular
        .module("UploadApp")
        .controller("UploadController", UploadController);
        
        UploadController.$inject = ["Upload"];
    
    function UploadController(Upload){
        var self = this;
        self.imgFile  = null;
        self.status = {
            message: "",
            code: 0
        }
        self.upload = upload;
        self.uploadS3 = uploadS3;

        function upload(){
            console.log("Uploading ...");
            Upload.upload({
                url: '/upload',
                data: {
                    "img-file": self.imgFile
                }
            }).then((result)=>{
                self.fileURL = result.data;
                self.status.message= "Upload Successfully ";
                self.status.code = result.status;
            }).catch((error)=>{
                self.status.message="Failed"
                self.status.code = err.status
            })
        }

        function uploadS3(){
            console.log("Uploading s3 ...");
            Upload.upload({
                url: '/uploadS3',
                data: {
                    "img-file": self.imgFile
                }
            }).then((result)=>{
                self.fileURL = result.data;
                self.status.message= "Upload Successfully ";
                self.status.code = result.status;
            }).catch((error)=>{
                self.status.message="Failed"
                self.status.code = err.status
            })
        }
    }

})();